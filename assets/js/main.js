(function ($) {
    $(function () {

        // Initialization Slider
        var sliderVideo = $('.slider.video').bxSlider({
            video: true,
            useCSS: false,
            controls: true,
            pagerCustom: '.list-video'
        });
        var sliderRev = $('.rev-slider').bxSlider({
            controls: true
        });
        var sliderProccess = $('.slider-process').bxSlider({
            controls: true
        });
        var sliderRevSt = $('.rev-st').bxSlider({
            controls: true
        });
        // Перезагружаем слайдер при смене ориентации
        var mql = window.matchMedia("(orientation: portrait)");
        mql.addListener(function (m) {
            // Пока не требуется!
        });

        // Показываем видео Виды танцев
        $('.slider.video li a').on('click', function (e) {
            e.preventDefault();
            link = $(this).attr('href');
            var parent = $(this).parents('li');
            videoWidth = parent.width();
            videoHeight = parent.height();
            parent.addClass('loading');
            parent.html(getPatternVideo(videoWidth, videoHeight, link));
        });

        // Показываем видео тренеров
        $('.video a').on('click', function (e) {
            e.preventDefault();
            link = $(this).attr('href');
            var parent = $(this).parents('.video');
            videoWidth = parent.width();
            videoHeight = parent.height();
            parent.css({
                'width': videoWidth,
                'height': videoHeight
            });
            parent.addClass('loading');
            parent.html(getPatternVideo(videoWidth, videoHeight, link));
        });

        // Действия при клике на иконки адреса и телефона
        var bWidth = $(window).width();
        if (bWidth <= 840) {
            var addressBlock = '.header-center';
            var phoneBlock = '.header-right';
            var $address = $('.header-center address');
            var $phone = $('.header-right .link-phone');

            $(addressBlock + ', ' + phoneBlock).on('click touchenter', function () {

                // Если ктото был открыт - закрываем
                if ($(this).hasClass('active')) {
                    $('.owerlay-bg').remove();
                    $('header').css('background', 'transparent');
                    //getPatternPhone($phone, 'hide');
                    $phone.hide();
                    $(this).removeClass('active');
                } else {
                    $(addressBlock + ', ' + phoneBlock).each(function () {
                        if ($(this).hasClass('active')) {
                            $(this).removeClass('active');
                            var classes = '.' + $(this).attr('class');
                            if (classes == phoneBlock) {
                                $phone.hide();
                            }
                        }
                    });
                    if ($('.owerlay-bg').length != 1) {
                        //$('header').css('background', '#f9f9f9')
                        $('body').append('<div class="owerlay-bg"></div>');
                    }

                    if ($(this).hasClass('header-right')) {
                        //getPatternPhone($phone, 'show');
                        $phone.show();
                    }

                    $(this).addClass('active');
                }
            });
        }

    })
})(jQuery);

function getPatternVideo(videoWidth, videoHeight, link) {
    return '<iframe width="' + videoWidth + '" height="' + videoHeight + '" src="' + link + '?autoplay=1" frameborder="0" allowfullscreen></iframe>';
}

/*function getPatternPhone(elem, type) {

    var number = elem.text();
    var parent = elem.parent();

    if (type == 'show') {
        $('.header-right > span').remove();
        $('.header-right').prepend('<a style="display:block;color:#ffffff;" href="tel:' + number + '"><span>' + number + '</span></a>');
    }

    if (type == 'hide') {
        $('header a[href^="tel:"]').remove();
        $('.header-right').prepend('<span>' + number + '</span>')
    }
}*/
